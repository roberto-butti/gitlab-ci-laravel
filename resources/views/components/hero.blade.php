<section class="hero is-info">
  <div class="hero-body">
    <p class="title">
      {{ $title ?? 'GitLab CI and Laravel' }}
    </p>
    <p class="subtitle">
      Creating your Gitlab CI Pipeline with <a href="https://gitlab.com/roberto-butti/gitlab-ci-laravel/-/blob/develop/.gitlab-ci.yml">gitlab-ci.yml</a> .
    </p>
  </div>
</section>
