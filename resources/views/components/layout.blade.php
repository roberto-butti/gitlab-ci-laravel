<html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css">
        <title>{{ $title ?? 'GitLab CI and Laravel' }}</title>
    </head>
    <body>
        <x-hero />

        {{ $slot }}
        <x-footer />
    </body>
</html>
