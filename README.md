
# Gitlab CI Laravel

## .gitlab-ci.yml

I'm playing with Gitlab CI for Laravel.

### Docs
- Quick Start: https://docs.gitlab.com/ee/ci/quick_start/
- Yaml .gitlab-ci.yml reference: https://docs.gitlab.com/ee/ci/yaml/README.html

### Useful files
The list of some useful file for code quality / automation:

- [.gitlab-ci.yml](https://gitlab.com/roberto-butti/gitlab-ci-laravel/-/blob/develop/.gitlab-ci.yml): the main Pipeline configuration file;
- [phpstan.neon](https://gitlab.com/roberto-butti/gitlab-ci-laravel/-/blob/develop/phpstan.neon): the configuration file for phpstan execution. With the file you could ensure the consistency across different context of execution. For example in your Gitlab CI, in your command line or your editor;
- [phpstan-laravel-baseline.neon](https://gitlab.com/roberto-butti/gitlab-ci-laravel/-/blob/develop/phpstan-laravel-baseline.neon): using "baseline" approach by PHPStan we can ignore some error raised by specific Laravel file. My suggestion is to focus on your own file, on that files that you can fully manage. [PHPStan and baseline](https://phpstan.org/user-guide/baseline).

### TODO list

- [x] Test with PHP8 and PHP7.4
- [x] trigger on push for "master", "develop" and "features/*" branches
- [x] enable mysql service
- [ ] cache npm modules
- [x] cache vendors
- [x] copy .env.example into .env
- [x] generate key for Laravel
- [ ] fix permission on cache directories
- [ ] show laravel version
- [x] execute tests
- [x] execute code sniffer via phpcs
- [ ] execute Code Static Analysis (PHP Stan + Laravel)
- [x] create automatically Json artifact for Gitlab Code Quality





## Moving from Github Actions
I created a tool for generating workflow for Github Actions:
https://ghygen.hi-folks.dev

My goal is to create a generator also for Gitlab CI.
As reference I would start from this Laravel CI configuration:
https://ghygen.hi-folks.dev?code=59785cd7a0dfee2c9202c470f4d348f6
