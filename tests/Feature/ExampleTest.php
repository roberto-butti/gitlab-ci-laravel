<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A basic user test.
     *
     * @return void
     */
    public function testUserTest()
    {
        $users = User::all();
        $this->assertCount(0, $users);
    }

    /**
     * A basic user test.
     *
     * @return void
     */
    public function testUserCreateTest()
    {
        $users = User::all();
        $this->assertCount(0, $users);
        User::create([
            'email' => 'foo@bar.com',
            'name' => 'Foo Bar',
            'password' => 'aaaaaaaaa'
        ]);
        $users = User::all();
        $this->assertCount(1, $users);
        $u = User::where('email', 'foo@bar.com')->first();
        $this->assertEquals("Foo Bar", $u->name);

    }
}
